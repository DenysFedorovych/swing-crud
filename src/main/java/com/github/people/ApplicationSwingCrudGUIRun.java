package com.github.people;

import com.github.people.config.ViewConfig;

public class ApplicationSwingCrudGUIRun {

    public static void main(String[] args) {
        ViewConfig.peopleFrame().getContentPane();
    }

}
