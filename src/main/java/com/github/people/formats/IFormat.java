package com.github.people.formats;

import com.github.people.entity.Person;

import java.util.List;

public interface IFormat{

    List<Person> fromFormat(String file);

    String toFormat(List<Person> person);

}
