package com.github.people.formats.impl;

import com.github.people.entity.Person;
import com.github.people.formats.IFormat;
import com.github.people.utils.PatternMatcher;
import com.mongodb.lang.NonNull;

import java.util.ArrayList;
import java.util.List;

public class JsonFormat implements IFormat {

    public List<Person> fromFormat(String file) {

        List<Person> personList = new ArrayList<>();
        if (!PatternMatcher.isBlank(file)) {
            List<String> personJsonList = PatternMatcher.jsonToPersonList(file);
            for (String each : personJsonList) {
                personList.add(this.toPerson(each));
            }
        }

        return personList;
    }


    public Person toPerson(String one) {

        String[] param = PatternMatcher.removeAllJsonSyntax(one).split(",");

        long id = 0;
        String firstName = "";
        String lastName = "";
        int age = 0;
        String city = "";

        for (String each : param) {
            int i = each.indexOf(':');
            switch (each.substring(0, i)) {
                case "id":
                    id = Long.parseLong(each.substring(i + 1));
                    break;
                case "firstName":
                    firstName = each.substring(i + 1);
                    break;
                case "lastName":
                    lastName = each.substring(i + 1);
                    break;
                case "age":
                    age = Integer.parseInt(each.substring(i + 1));
                    break;
                case "city":
                    city = each.substring(i + 1);
                    break;
            }
        }

        return new Person(id, firstName, lastName, age, city);
    }


    public String toFormat(@NonNull List<Person> people) {
        if (people.size() == 0) {
            return "{}";
        }

        StringBuilder result = new StringBuilder();
        if (people.size() == 1) {
            result.append("{\"person\":");
            result.append(toFormat(people.get(0)));
            result.append("}");
        } else {
            result.append("{\"people\": [\n");
            for (Person each : people) {
                result.append(toFormat(each));
                result.append(",\n");
            }
            result.deleteCharAt(result.length() - 2);
            result.append("]}");
        }

        return result.toString();
    }

    public String toFormat(@NonNull Person person){
        return String.format("{\n \t\"id\": %d,\n\t\"firstName\": \"%s\",\n\t\"lastName\": \"%s\",\n\t\"age\": %d,\n\t\"city\": \"%s\"\n}",
                person.getId(),
                person.getFirstName(),
                person.getLastName(),
                person.getAge(),
                person.getCity()
        );
    }

}
