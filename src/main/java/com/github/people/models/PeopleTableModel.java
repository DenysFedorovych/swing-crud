package com.github.people.models;

import com.github.people.config.ServicesConfig;
import com.github.people.entity.Person;
import com.github.people.services.IPeopleService;
import com.github.people.utils.PersonColumn;
import com.github.people.utils.ReflectionUtils;

import javax.swing.table.AbstractTableModel;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class PeopleTableModel extends AbstractTableModel {

    private List<Person> people = new ArrayList<>();

    private IPeopleService peopleService;

    private final List<Field> tableColumns;

    public PeopleTableModel(IPeopleService peopleService, List<Field> tableColumns) {
        this.peopleService = peopleService;
        this.tableColumns = tableColumns;
    }

    public PeopleTableModel refresh() {
        this.people = this.peopleService.readAll();
        return this;
    }

    public boolean isEmpty(){
        return this.peopleService == ServicesConfig.startingService;
    }

    public PeopleTableModel delete(Person person){
        this.peopleService.delete(person.getId());
        this.refresh();
        return this;
    }

    public PeopleTableModel create(Person person){
        person.setId(this.getMaxId()+1);
        this.peopleService.create(person);
        this.refresh();
        return this;
    }

    private long getMaxId(){
        long maxId = 0;
        for(Person each : this.people){
            maxId = Math.max(maxId,each.getId());
        }
        return maxId;
    }

    public PeopleTableModel update(Person person){
        this.peopleService.update(person);
        this.refresh();
        return this;
    }

    public void setPeopleService(IPeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @Override
    public int getRowCount() {
        return this.people.size();
    }

    @Override
    public int getColumnCount() {
        return this.tableColumns.size();
    }

    @Override
    public String getColumnName(int col) {
        return this.tableColumns.get(col)
                .getAnnotation(PersonColumn.class).name();
    }

    @Override
    public Object getValueAt(int row, int col) {
        Person p = this.people.get(row);
        Field field = this.tableColumns.get(col);
        return ReflectionUtils.getValue(field, p);
    }
}
