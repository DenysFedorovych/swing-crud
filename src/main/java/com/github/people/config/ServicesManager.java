package com.github.people.config;

import com.github.people.services.IPeopleService;

import java.io.File;

public class ServicesManager {

    public static IPeopleService getService(File file) {
        return ServicesConfig.formatPersonService(file);
    }

    public static IPeopleService getService(String dbName) {
        switch (dbName) {
            case "MySQL":
                return ServicesConfig.mySQLPersonService();
            case "PostgreSQL":
                return ServicesConfig.postgreSQLPersonService();
            case "H2":
                return ServicesConfig.h2PersonService();
            case "MongoDB":
                return ServicesConfig.mongoDBPersonService();
            case "GraphDB":
                return ServicesConfig.graphDBPersonService();
            case "Cassandra":
                return ServicesConfig.cassandraPersonService();
            case "Redis":
                return ServicesConfig.redisPersonService();
            default:
                throw new IllegalArgumentException("Extension of a file is not supported.");
        }
    }

}
