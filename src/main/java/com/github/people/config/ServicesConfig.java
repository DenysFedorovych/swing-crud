package com.github.people.config;

import com.github.people.services.impl.database.*;
import com.github.people.services.impl.files.*;
import com.github.people.services.IPeopleService;
import com.github.people.utils.ServiceHandler;

import java.io.File;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class ServicesConfig {

    public static MockService startingService = new MockService();

    public static IPeopleService formatPersonService(File file) {
        IPeopleService origin = FormatService.getFormatService(file);
        InvocationHandler handler = new ServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler);
    }

    public static IPeopleService mySQLPersonService() {
        IPeopleService origin = MySqlService.getMySQLService(RepositoryConfig.peopleRepoMySql());
        InvocationHandler handler = new ServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler);
    }

    public static IPeopleService h2PersonService() {
        IPeopleService origin = H2Service.getH2Service(RepositoryConfig.peopleRepoH2());
        InvocationHandler handler = new ServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler);
    }

    public static IPeopleService cassandraPersonService() {
        IPeopleService origin = CassandraService.getCassandraService(RepositoryConfig.peopleRepoCassandra());
        InvocationHandler handler = new ServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler);
    }

    public static IPeopleService redisPersonService() {
        IPeopleService origin = RedisService.getRedisService(RepositoryConfig.peopleRepoRedis());
        InvocationHandler handler = new ServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler);
    }

    public static IPeopleService mongoDBPersonService() {
        IPeopleService origin = MongoDBService.getMongoDBService(RepositoryConfig.peopleRepoMongoDB());
        InvocationHandler handler = new ServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler);
    }

    public static IPeopleService postgreSQLPersonService() {
        IPeopleService origin = PostgreSQLService.getPostgreSQLService(RepositoryConfig.peopleRepoPg());
        InvocationHandler handler = new ServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler);
    }

    public static IPeopleService graphDBPersonService() {
        IPeopleService origin = GraphDBService.getGraphDBService(RepositoryConfig.peopleRepoGraphDB());
        InvocationHandler handler = new ServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler);
    }

}
