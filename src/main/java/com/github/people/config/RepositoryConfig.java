package com.github.people.config;

import com.github.people.repository.IPeopleRepo;
import com.github.people.repository.impl.database.*;
import com.github.people.utils.JDBConnectionPool;

public class RepositoryConfig {

    public static IPeopleRepo peopleRepoPg() {
        return PeoplePgRepo.getPgRepo(new JDBConnectionPool(
                30000,
                "org.postgresql.Driver",
                "jdbc:postgresql://localhost:5432/postgres",
                "postgres",
                "postgres"
        ));
    }

    public static IPeopleRepo peopleRepoMySql() {
        return PeopleMySqlRepo.getMySQLRepo(new JDBConnectionPool(
                30000,
                "com.mysql.jdbc.Driver",
                "jdbc:mysql://localhost:3306/people_bd",
                "root",
                "my-secret-pw"
        ));
    }

    public static IPeopleRepo peopleRepoCassandra() {
        return new PeopleCassandraRepo(
                "127.0.0.1",
                "",
                "",
                9042,
                "people");
    }

    public static IPeopleRepo peopleRepoH2() {
        return new PeopleH2Repo(new JDBConnectionPool(
                30000,
                "org.h2.Driver",
                "jdbc:h2:~/test",
                "sa",
                ""));
    }

    public static IPeopleRepo peopleRepoMongoDB() {
        return new PeopleMongoDbRepo(
                "localhost",
                27017,
                "mongo",
                "password",
                "people");
    }


    public static IPeopleRepo peopleRepoGraphDB() {
        return new PeopleGraphDBRepo();
    }

    public static IPeopleRepo peopleRepoRedis() {
        return PeopleRedisRepo.getRedisRepo();
    }
}
