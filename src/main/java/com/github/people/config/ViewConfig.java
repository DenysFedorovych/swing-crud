package com.github.people.config;

import com.github.people.entity.Person;
import com.github.people.models.PeopleTableModel;
import com.github.people.services.impl.database.MockService;
import com.github.people.view.panels.PeopleCrudCommandsPanel;
import com.github.people.utils.ReflectionUtils;
import com.github.people.view.PeopleForm;
import com.github.people.view.panels.PeopleTablePanel;

import javax.swing.*;
import java.lang.reflect.Field;
import java.util.List;

public class ViewConfig {

    private static List<Field> personColumns = ReflectionUtils.fields(Person.class);

    private static PeopleTableModel startTable =
            new PeopleTableModel(ServicesConfig.startingService, personColumns)
                    .refresh();

    private static PeopleCrudCommandsPanel peopleCrudCommandsPanel() {
        return new PeopleCrudCommandsPanel();
    }

    private static PeopleTablePanel peopleTablePanel() {
        return new PeopleTablePanel(startTable);
    }

    public static JFrame peopleFrame() {

        PeopleCrudCommandsPanel crudCmd = peopleCrudCommandsPanel();
        PeopleTablePanel peopleTablePanel = peopleTablePanel();
        PeopleForm peopleForm = new PeopleForm(peopleTablePanel, crudCmd);
        crudCmd.setCrudCommands(peopleForm);
        return peopleForm;

    }

}
