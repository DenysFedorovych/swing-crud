package com.github.people.view.dialogs.read;

import com.github.people.services.IPeopleService;

import javax.swing.*;
import java.awt.event.ActionListener;

public class ReadDBDialog extends JDialog {

    private String[] databasesArr = {"MySQL", "PostgreSQL", "H2", "MongoDB", "GraphDB", "Cassandra", "Redis"};

    private final JComboBox comboBox = new JComboBox(databasesArr);

    private final JButton submit = new JButton("Confirm");

    private final JButton cancel = new JButton("Cancel");

    private boolean isEnded = false;

    public String getValue() {
        return (String) this.comboBox.getSelectedItem();
    }


    public ReadDBDialog() {

        this.setTitle("Database chooser");
        this.setModal(true);
        this.setLayout(null);
        this.setBounds(300, 300, 300, 200);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.comboBox.setBounds(50, 25, 200, 35);
        this.submit.setBounds(40, 120, 100, 25);
        this.cancel.setBounds(160, 120, 100, 25);

        this.add(submit);
        this.add(cancel);
        this.add(comboBox);

        this.submit.addActionListener(actionConfirm());
        this.cancel.addActionListener(actionCancel());

        setVisible(Boolean.TRUE);
    }

    public String getDBName() {
        return String.valueOf(comboBox.getSelectedItem());
    }

    public ActionListener actionConfirm() {
        return e -> {
            isEnded = true;
            this.setVisible(Boolean.FALSE);
        };
    }

    public ActionListener actionCancel() {
        return e -> {
            isEnded = false;
            this.setVisible(Boolean.FALSE);
        };
    }

    public boolean isEnded() {
        return isEnded;
    }
}
