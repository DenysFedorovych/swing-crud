package com.github.people.view.dialogs.update;

import com.github.people.entity.Person;
import com.github.people.utils.PatternMatcher;
import com.github.people.view.Warnings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

public class UpdateDialog extends JDialog {

    private boolean isEnded = false;

    private Person person;

    private final long personID;

    private final JLabel firstNameLabel = new JLabel("First name");
    private final JTextField firstNameField = new JTextField();

    private final JLabel lastNameLabel = new JLabel("First name");
    private final JTextField lastNameField = new JTextField();

    private final JLabel ageLabel = new JLabel("Age");
    private final JTextField ageField = new JTextField();

    private final JLabel cityLabel = new JLabel("City");
    private final JTextField cityField = new JTextField();

    public final JButton submitButton = new JButton("Confirm");
    public final JButton cancelButton = new JButton("Cancel");

    public UpdateDialog(Person person) {

        this.setTitle("Update person");
        this.setModal(true);
        this.setLayout(null);
        this.setBounds(300, 300, 300, 300);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.firstNameField.setBounds(120, 30, 140, 30);
        this.lastNameField.setBounds(120, 70, 140, 30);
        this.ageField.setBounds(120, 110, 140, 30);
        this.cityField.setBounds(120, 150, 140, 30);

        this.firstNameLabel.setBounds(40, 30, 70, 30);
        this.lastNameLabel.setBounds(40, 70, 70, 30);
        this.ageLabel.setBounds(40, 110, 70, 30);
        this.cityLabel.setBounds(40, 150, 70, 30);

        this.personID = person.getId();
        this.firstNameField.setText(person.getFirstName());
        this.lastNameField.setText(person.getLastName());
        this.ageField.setText(Integer.toString(person.getAge()));
        this.cityField.setText(person.getCity());

        this.submitButton.setBounds(40, 200, 100, 25);
        this.cancelButton.setBounds(160, 200, 100, 25);

        submitButton.addActionListener(e -> {
            if (validates()) {
                this.isEnded = true;
                String firstName = this.firstNameField.getText();
                String lastName = this.lastNameField.getText();
                int age = Integer.parseInt(this.ageField.getText());
                String city = this.cityField.getText();
                this.person = new Person(personID, firstName, lastName, age, city);
                this.isEnded = true;
                this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
            } else {
                Warnings.showCreatePersonWarning();
            }
        });

        cancelButton.addActionListener(e -> {
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        });

        this.add(firstNameField);
        this.add(lastNameField);
        this.add(ageField);
        this.add(cityField);

        this.add(firstNameLabel);
        this.add(lastNameLabel);
        this.add(ageLabel);
        this.add(cityLabel);

        this.add(submitButton);
        this.add(cancelButton);

        setVisible(Boolean.TRUE);

    }

    private boolean validates() {
        return PatternMatcher.isValidName(this.firstNameField.getText()) &&
                PatternMatcher.isValidName(this.lastNameField.getText()) &&
                PatternMatcher.isValidAge(this.ageField.getText()) &&
                PatternMatcher.isValidName(this.cityField.getText());
    }

    public Person getUpdatedPerson() {
        return this.person;
    }

    public boolean isEnded() {
        return this.isEnded;
    }

}
