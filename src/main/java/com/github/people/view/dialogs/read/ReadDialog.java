package com.github.people.view.dialogs.read;

import com.github.people.utils.file_utils.Paths;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public class ReadDialog {

    private final String[] extensions = {"json","csv","yml","xml","bin"};

    private FileNameExtensionFilter filter = new FileNameExtensionFilter("Allowed formats",extensions);

    private File file;

    private boolean isEnded = false;

    public ReadDialog(){
        JFileChooser jFileChooser = new JFileChooser(Paths.getPersonBasePath());
        jFileChooser.setFileFilter(filter);
        int result = jFileChooser.showOpenDialog(new JFrame());
        if(result == JFileChooser.APPROVE_OPTION){this.isEnded = true;}
        file = jFileChooser.getSelectedFile();
    }

    public boolean isEnded() {
        return isEnded;
    }

    public File getFile(){
        return this.file;
    }

}
