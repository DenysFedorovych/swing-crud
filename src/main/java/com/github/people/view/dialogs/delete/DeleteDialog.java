package com.github.people.view.dialogs.delete;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class DeleteDialog extends JDialog {

    private final JButton submitButton = new JButton("Yes");

    private final JButton cancelButton = new JButton("No");

    private final JLabel label = new JLabel("Are you sure you want delete this person?");

    private boolean isConfirmed = false;

    public DeleteDialog() {

        this.setTitle("Delete");
        this.setModal(true);
        this.setLayout(null);
        this.setBounds(400, 300, 370, 150);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.submitButton.setBounds(60, 80, 100, 30);
        this.cancelButton.setBounds(210, 80, 100, 30);
        this.label.setBounds(50, 20, 400, 50);

        this.submitButton.addActionListener(e -> {
            isConfirmed = true;
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        });

        this.cancelButton.addActionListener(e -> {
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        });

        this.add(label);
        this.add(submitButton);
        this.add(cancelButton);

        setVisible(Boolean.TRUE);
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }
}
