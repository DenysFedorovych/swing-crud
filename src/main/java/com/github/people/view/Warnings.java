package com.github.people.view;

import javax.swing.*;

public class Warnings {

    public static void showCreatePersonWarning(){
        JOptionPane.showMessageDialog(new JFrame(), "Wrong input data, try again", "Something wrong", JOptionPane.WARNING_MESSAGE);
    }

    public static void showNullTableWarning(){
        JOptionPane.showMessageDialog(new JFrame(), "Choose data to read before create", "Warning", JOptionPane.WARNING_MESSAGE);
    }

    public static void showNoSelectedPersonWarning(){
        JOptionPane.showMessageDialog(new JFrame(), "Choose person before command", "Warning", JOptionPane.WARNING_MESSAGE);
    }

    public static void showDatabaseNotEnabledWarning(){
        JOptionPane.showMessageDialog(new JFrame(), "Chosen database is not running", "Warning", JOptionPane.WARNING_MESSAGE);
    }
}
