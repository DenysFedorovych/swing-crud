package com.github.people.view;

import com.github.people.view.panels.PeopleCrudCommandsPanel;
import com.github.people.view.panels.PeopleTablePanel;

import javax.swing.*;
import java.awt.*;

public class PeopleForm extends JFrame {

    private PeopleTablePanel peopleTable;

    private PeopleCrudCommandsPanel peopleCommands;

    public PeopleForm(
            PeopleTablePanel peopleTablePanel,
            PeopleCrudCommandsPanel crudCmd) throws HeadlessException {

        this.peopleTable = peopleTablePanel;
        this.peopleCommands = crudCmd;

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setBounds(200,100,800, 600);
        setBackground(new Color(116, 69, 69));
        setTitle("Person Database");
        add(peopleTablePanel);
        add(crudCmd);
        setVisible(Boolean.TRUE);
    }

    public PeopleTablePanel getPeopleTable() {
        return peopleTable;
    }
}
