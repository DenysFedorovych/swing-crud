package com.github.people.view.panels;

import com.github.people.commands.CrudCommands;
import com.github.people.view.PeopleForm;

import javax.swing.*;
import java.awt.*;


public class PeopleCrudCommandsPanel extends JPanel {

    private CrudCommands crudCommands;

    public void setCrudCommands(PeopleForm peopleForm) {
        this.crudCommands = new CrudCommands(peopleForm);
    }

    public PeopleCrudCommandsPanel() {

        setLayout(null);
        setBackground(new Color(106, 34, 56));

        setBounds(600, 0, 200, 600);

        JButton btnCreate = new JButton("Create");
        JButton btnReadFile = new JButton("Read File");
        JButton btnReadDB = new JButton("Read Data");
        JButton btnUpdate = new JButton("Update");
        JButton btnDelete = new JButton("Remove");

        btnCreate.setBounds(10, 10 ,180, 40);
        btnReadFile.setBounds(10, 80 ,180, 40);
        btnReadDB.setBounds(10,150,180,40);
        btnUpdate.setBounds(10, 220 ,180, 40);
        btnDelete.setBounds(10, 290 ,180, 40);

        btnReadFile.addActionListener(e -> crudCommands.readFile());
        btnReadDB.addActionListener(e -> crudCommands.readDataBase());
        btnCreate.addActionListener(e -> crudCommands.create());
        btnDelete.addActionListener(e -> crudCommands.delete());
        btnUpdate.addActionListener(e -> crudCommands.update());

        add(btnCreate);
        add(btnReadFile);
        add(btnReadDB);
        add(btnUpdate);
        add(btnDelete);

        setVisible(Boolean.TRUE);
    }
}
