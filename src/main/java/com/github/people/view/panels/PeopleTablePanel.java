package com.github.people.view.panels;

import com.github.people.entity.Person;
import com.github.people.models.PeopleTableModel;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.lang.reflect.Field;

public class PeopleTablePanel extends JPanel {

    private JTable peopleTable;

    private PeopleTableModel peopleTableModel;

    public PeopleTablePanel(PeopleTableModel peopleTableModel) {
        setLayout(null);
        setBounds(0, 0, 600, 600);

        this.peopleTable = new JTable((TableModel) peopleTableModel);
        this.peopleTableModel = peopleTableModel;
        JScrollPane scr = new JScrollPane(this.peopleTable);

        scr.setBounds(5, 5, 590, 550);
        add(scr);

        setVisible(Boolean.TRUE);
    }

    public Person getSelectedPerson(){

        int row = this.peopleTable.getSelectedRow();
        if(row == -1) {return null;}

        Person person = new Person();
        Class<? extends Person> clz = person.getClass();
        Field[] fields = clz.getDeclaredFields();
        try {
            for (int i = 1; i < fields.length; i++) {
                fields[i].setAccessible(true);
                Object tmp = this.peopleTableModel.getValueAt(row, i - 1);
                if (long.class.equals(fields[i].getType())) {
                    fields[i].setLong(person, Long.parseLong(String.valueOf(tmp)));
                }
                if (int.class.equals(fields[i].getType())) {
                    fields[i].setInt(person, Integer.parseInt(String.valueOf(tmp)));
                }
                if (String.class.equals(fields[i].getType())) {
                    fields[i].set(person, String.valueOf(tmp));
                }
            }
        } catch (IllegalAccessException illegalAccessException) {
           illegalAccessException.printStackTrace();
        }
        return person;

    }

    public JTable getPeopleTable() {
        return peopleTable;
    }

    public PeopleTableModel getPeopleTableModel() {
        return peopleTableModel;
    }

    public boolean haveSelectedPerson(){
        return this.getSelectedPerson() != null;
    }
}
