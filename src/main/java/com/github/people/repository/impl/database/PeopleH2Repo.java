package com.github.people.repository.impl.database;

import com.github.people.entity.Person;
import com.github.people.repository.IPeopleRepo;
import com.github.people.utils.JDBConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PeopleH2Repo implements IPeopleRepo {
    private static final Logger log = LoggerFactory.getLogger(PeopleH2Repo.class);

    private JDBConnectionPool pool;

    public PeopleH2Repo(JDBConnectionPool pool) {
        this.pool = pool;
    }

    @Override
    public Person save(Person p) {
        long id = 0;
        Connection con = this.pool.connection();
        String sql = "insert into PEOPLE (first_name, last_name, age, city) values(?, ?, ?, ?)";
        try (PreparedStatement stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setString(1, p.getFirstName());
            stmt.setString(2, p.getLastName());
            stmt.setInt(3, p.getAge());
            stmt.setString(4, p.getCity());
            int row = stmt.executeUpdate();
            if (row != 0) {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            log.warn("Enter: {}" + e.getMessage());
        } finally {
            this.pool.parking(con);
        }
        return p.id(id);
    }

    @Override
    public List<Person> findAll() {
        List<Person> result = new ArrayList<>();
        Connection con = this.pool.connection();
        String sql = "select * from PEOPLE";
        try (PreparedStatement stmt = con.prepareStatement(sql)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Person p = new Person(
                        rs.getLong("id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getInt("age"),
                        rs.getString("city")
                );
                result.add(p);
            }
        } catch (SQLException e) {
            log.warn("Enter: {}" + e.getMessage());
        } finally {
            this.pool.parking(con);
        }
        return result;
    }

    @Override
    public void update(Person p) {
        long id = 0;
        Connection con = this.pool.connection();
        String sql = "update PEOPLE " +
                "set first_name = ?, last_name = ?, age = ?, city = ?" +
                "where id = ?";
        try (PreparedStatement stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setString(1, p.getFirstName());
            stmt.setString(2, p.getLastName());
            stmt.setInt(3, p.getAge());
            stmt.setString(4, p.getCity());
            stmt.setLong(5, p.getId());
            int row = stmt.executeUpdate();
            if (row != 0) {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            log.warn("Enter: {}" + e.getMessage());
        } finally {
            this.pool.parking(con);
        }
        p.id(id);
    }

    @Override
    public void remove(long id) {
        Connection con = this.pool.connection();
        String sql = "delete from PEOPLE where id = ?";
        try (PreparedStatement stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setLong(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            log.warn("Enter: {}" + e.getMessage());
        } finally {
            this.pool.parking(con);
        }
    }
}
