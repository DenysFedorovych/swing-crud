package com.github.people.repository.impl.database;

import com.github.people.entity.Person;
import com.github.people.repository.IPeopleRepo;

import java.util.List;

public class PeopleGraphDBRepo implements IPeopleRepo {
    @Override
    public Person save(Person p) {
        return null;
    }

    @Override
    public List<Person> findAll() {
        return null;
    }

    @Override
    public void update(Person p) {

    }

    @Override
    public void remove(long id) {

    }
}
