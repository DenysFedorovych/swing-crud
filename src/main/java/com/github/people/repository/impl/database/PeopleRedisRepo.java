package com.github.people.repository.impl.database;

import com.github.people.entity.Person;
import com.github.people.formats.impl.JsonFormat;
import com.github.people.repository.IPeopleRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class PeopleRedisRepo implements IPeopleRepo {

    private final JsonFormat jsonFormat = new JsonFormat();

    private Jedis jedis;

    private static final Logger log = LoggerFactory.getLogger(PeopleMySqlRepo.class);

    private List<Person> people;

    private static PeopleRedisRepo classObject;

    private PeopleRedisRepo() {
        this.jedis = new Jedis("localhost",6379);
        this.jedis.connect();
        this.jedis.sadd("people","");
    }

    public static PeopleRedisRepo getRedisRepo() {
        if (classObject == null) {
            classObject = new PeopleRedisRepo();
        }
        return classObject;
    }

    @Override
    public Person save(Person p) {
        this.jedis.sadd("people",this.jsonFormat.toFormat(p));
        this.people.add(p);
        return new Person(p);
    }

    @Override
    public List<Person> findAll() {
        List<Person> result = new ArrayList<>();
        for(String each : jedis.smembers("people")){
            if(each.equals("")) continue;
            result.add(jsonFormat.fromFormat(each).get(0));
        }
        this.people = result;
        return people;
    }

    @Override
    public void update(Person p) {
        for(Person each : people){
            if(each.getId() == p.getId()){
                this.jedis.srem("people",this.jsonFormat.toFormat(each));
                this.people.remove(each);
                this.jedis.sadd("people",this.jsonFormat.toFormat(p));
                this.people.add(p);
                return;
            }
        }
    }

    @Override
    public void remove(long id) {
        for(Person each : people){
            if(each.getId() == id){
                this.jedis.srem("people",this.jsonFormat.toFormat(each));
                this.people.remove(each);
                return;
            }
        }
    }
}
