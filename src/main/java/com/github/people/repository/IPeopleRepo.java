package com.github.people.repository;

import com.github.people.entity.Person;

import java.util.List;

public interface IPeopleRepo {
    Person save(Person p);

    List<Person> findAll();

    void update(Person p);

    void remove(long id);
}
