package com.github.people.commands;

import com.github.people.config.ServicesManager;
import com.github.people.entity.Person;
import com.github.people.services.IPeopleService;
import com.github.people.view.PeopleForm;
import com.github.people.view.Warnings;
import com.github.people.view.dialogs.create.CreateDialog;
import com.github.people.view.dialogs.delete.DeleteDialog;
import com.github.people.view.dialogs.read.ReadDBDialog;
import com.github.people.view.dialogs.read.ReadDialog;
import com.github.people.view.dialogs.update.UpdateDialog;
import com.github.people.view.panels.PeopleTablePanel;

import java.io.File;
import java.lang.reflect.UndeclaredThrowableException;

public class CrudCommands {

    private PeopleForm peopleForm;

    private PeopleTablePanel tablePanel;

    public CrudCommands(PeopleForm peopleForm) {
        this.peopleForm = peopleForm;
        this.tablePanel = this.peopleForm.getPeopleTable();
    }

    private void remake() {
        tablePanel.getPeopleTableModel().refresh();
        tablePanel.getPeopleTable().revalidate();
        tablePanel.repaint();
    }

    public void readFile() {
        ReadDialog readDialog = new ReadDialog();
        if (readDialog.isEnded()) {
            File file = readDialog.getFile();
            tablePanel.getPeopleTableModel().setPeopleService(ServicesManager.getService(file));
            remake();
        }
    }

    public void readDataBase() {
        ReadDBDialog dbDialog = new ReadDBDialog();
        if (dbDialog.isEnded()) {
            try {
                IPeopleService iPeopleService = ServicesManager.getService(dbDialog.getDBName());
                tablePanel.getPeopleTableModel().setPeopleService(iPeopleService);
                remake();
            } catch (UndeclaredThrowableException e) {
                Warnings.showDatabaseNotEnabledWarning();
            }
        }
    }

    public void delete() {
        if (this.tablePanel.haveSelectedPerson()) {
            DeleteDialog deleteDialog = new DeleteDialog();
            if (deleteDialog.isConfirmed()) {
                this.tablePanel.getPeopleTableModel().delete(this.tablePanel.getSelectedPerson());
                remake();
            }
        } else {
            Warnings.showNoSelectedPersonWarning();
        }
    }

    public void update() {
        if (this.tablePanel.haveSelectedPerson()) {
            UpdateDialog updateDialog = new UpdateDialog(this.tablePanel.getSelectedPerson());
            if (updateDialog.isEnded()) {
                this.tablePanel.getPeopleTableModel().update(updateDialog.getUpdatedPerson());
                remake();
            }
        } else {
            Warnings.showNoSelectedPersonWarning();
        }
    }

    public void create() {
        if (this.tablePanel.getPeopleTableModel().isEmpty()) {
            Warnings.showNullTableWarning();
        } else {
            CreateDialog createDialog = new CreateDialog();
            if (createDialog.isEnded()) {
                Person person = createDialog.getCreatedPerson();
                tablePanel.getPeopleTableModel().create(person);
                remake();
            }
        }
    }

}
