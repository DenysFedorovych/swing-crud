package com.github.people.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Arrays;

import java.util.List;
import java.util.stream.Collectors;


public class ReflectionUtils {

    private static final Logger logger = LoggerFactory.getLogger(ReflectionUtils.class);

    public static List<Field> fields(Class<?> clz) {

        Field[] fields = clz.getDeclaredFields();
        return Arrays.stream(fields)
                .filter(f -> f.isAnnotationPresent(PersonColumn.class))
                .collect(Collectors.toList());
    }

    public static Object getValue(Field field, Object p) {
        field.setAccessible(Boolean.TRUE);
        try {
            return field.get(p);
        } catch (IllegalAccessException e) {
            logger.warn("Reflection utils has access exception : " + e.getMessage());
        }
        return null;
    }

}
