package com.github.people.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class JDBConnectionPool {

    private static final Logger log = LoggerFactory.getLogger(JDBConnectionPool.class);

    private Long timeout;

    private String connectionString;

    private String url;
    private String usr;
    private String psw;

    private AtomicInteger count;

    private Map<Connection,ConnectionEntry> pool = new HashMap<>();

    public JDBConnectionPool(long timeout, String driver, String url, String usr, String psw) {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        this.timeout = timeout;
        this.url = url;
        this.usr = usr;
        this.psw = psw;
        this.count = new AtomicInteger(0);
    }

    public JDBConnectionPool(long timeout, String driver, String connectionString) {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        this.timeout = timeout;
        this.connectionString = connectionString;
        this.count = new AtomicInteger(0);
    }

    private Connection doConnect() {
        try {
            if(connectionString!=null) return DriverManager.getConnection(connectionString);
            return DriverManager.getConnection(this.url,this.usr,this.psw);
        } catch (SQLException e) {
            log.warn("Enter: {}" + e.getMessage());
        }
        return null;
    }

    private void expire(Connection con) {
        try {
            con.close();
        } catch (SQLException e) {
            log.warn("Enter: {}" + e.getMessage());
        }
    }

    private boolean validate (Connection con) {
        try {
            return !con.isClosed();
        } catch (SQLException e) {
            log.warn("Enter: {}" + e.getMessage());
        }
        return false;
    }

    public Connection connection() {
        long now = System.currentTimeMillis();
        for (Connection con : this.pool.keySet()){
            ConnectionEntry entry = this.pool.get(con);
            if ((now - entry.createAt) > this.timeout) {
                expire(con);
                this.pool.remove(con);
            }else {
                if (validate(con)) {
                    if (entry.status == ConnectionStatus.off) {
                        entry.status = ConnectionStatus.on;
                        log.info("Enter {}" + entry);
                        return con;
                    }
                }else {
                    expire(con);
                    this.pool.remove(con);
                }
            }
        }
        Connection con = doConnect();
        ConnectionEntry entry = new ConnectionEntry(this.count.incrementAndGet(), ConnectionStatus.on,now);
        log.info("Enter {}" + entry);
        this.pool.put(con,entry);
        return con;
    }

    public void parking(Connection con) {
        long now = System.currentTimeMillis();
        ConnectionEntry entry = this.pool.get(con);
        entry.status = ConnectionStatus.off;
        entry.createAt = now;
    }

    private static class ConnectionEntry {
        private int number;
        private ConnectionStatus status;
        private long createAt;

        public ConnectionEntry(int number, ConnectionStatus status, long createAt) {
            this.number = number;
            this.status = status;
            this.createAt = createAt;
        }

        @Override
        public String toString() {
            return "Connection number= " + number;
        }
    }

    private enum ConnectionStatus {
        on, off
    }
}
