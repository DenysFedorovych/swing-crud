package com.github.people.utils;

import com.github.people.services.IPeopleService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ServiceHandler implements InvocationHandler {

    private static final Logger log = LoggerFactory.getLogger(ServiceHandler.class);

    private final IPeopleService peopleService;

    public ServiceHandler(IPeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        log.info("Before call to method: {}, with args: {}", method.getName(), args);
        Object result = method.invoke(peopleService, args);
        log.info("After call to method: {}",  result);
        return result;
    }
}
