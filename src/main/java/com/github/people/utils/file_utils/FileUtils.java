package com.github.people.utils.file_utils;

import java.io.*;

public class FileUtils {

    public static boolean writeToFile(String fileName, String content) {
        if (content == null) {
            return false;
        }
        String path = Paths.definePath(fileName);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path, false))) {
            writer.write(content);
            return true;
        } catch (IOException ignore) {
        }
        return false;
    }

    public static String readFile(String fileName) {
        String path = Paths.definePath(fileName);
        StringBuilder fileContent = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!fileContent.toString().equals("")) {
                    fileContent.append("\r\n");
                }
                fileContent.append(line);
            }
        } catch (IOException ignore) {
        }
        return fileContent.toString();
    }


    public static boolean deleteFile(String fileName) {
        String path = Paths.definePath(fileName);
        File file = new File(path);
        if (file.exists() && file.isFile()) {
            return file.delete();
        } else {
            return false;
        }
    }


    public static byte[] readBinFile(String fileName) {
        String pathToFile = Paths.getBinBasePath() +
                fileName;
        File file = new File(pathToFile);
        if(!file.exists()){
            throw new IllegalArgumentException("File not exists");
        }
        FileInputStream fis;
        byte[] data = new byte[(int) file.length()];
        try{
            fis = new FileInputStream(file);
            if(fis.read(data) == -1){
                throw new IllegalArgumentException("Failed to read file");
            }
            fis.close();
            return data;
        } catch (IOException e){
            throw new IllegalArgumentException("Failed to read file");
        }
    }


    public static void writeToBinFile(byte[] data, String fileName) {
        String pathToFile = Paths.getBinBasePath() + fileName;
        File file = new File(pathToFile);
        FileOutputStream fos;
        try{
            fos = new FileOutputStream(file);
            fos.write(data);
            fos.close();
        } catch (IOException e){
            throw new IllegalArgumentException("Failed to write file");
        }
    }
}
