package com.github.people.services.impl.database;

import com.github.people.entity.Person;
import com.github.people.repository.IPeopleRepo;
import com.github.people.services.IPeopleService;

import java.util.List;

public class PostgreSQLService implements IPeopleService {

    private final IPeopleRepo peopleRepo;

    private static PostgreSQLService classObject;

    private PostgreSQLService(IPeopleRepo peopleRepo) {
        this.peopleRepo = peopleRepo;
    }

    public static PostgreSQLService getPostgreSQLService(IPeopleRepo peopleRepo){
        if(classObject == null){classObject = new PostgreSQLService(peopleRepo);}
        return classObject;
    }

    @Override
    public Person create(Person person) {
        return this.peopleRepo.save(person);
    }

    @Override
    public List<Person> readAll() {
        return this.peopleRepo.findAll();
    }

    @Override
    public void update(Person person) {
        this.peopleRepo.update(person);
    }

    @Override
    public void delete(long id) {
        this.peopleRepo.remove(id);
    }
}
