package com.github.people.services.impl.database;

import com.github.people.entity.Person;
import com.github.people.repository.IPeopleRepo;
import com.github.people.services.IPeopleService;

import java.util.List;

public class GraphDBService implements IPeopleService {

    private final IPeopleRepo peopleRepo;

    private static GraphDBService classObject;

    private GraphDBService(IPeopleRepo peopleRepo) {
        this.peopleRepo = peopleRepo;
    }

    public static GraphDBService getGraphDBService(IPeopleRepo peopleRepo){
        if(classObject == null){classObject = new GraphDBService(peopleRepo);}
        return classObject;
    }

    @Override
    public Person create(Person person) {
        return null;
    }

    @Override
    public List<Person> readAll() {
        return null;
    }

    @Override
    public void update(Person person) {

    }

    @Override
    public void delete(long id) {

    }
}
