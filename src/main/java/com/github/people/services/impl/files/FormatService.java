package com.github.people.services.impl.files;

import com.github.people.entity.Person;
import com.github.people.formats.IFormat;
import com.github.people.formats.impl.*;
import com.github.people.services.IPeopleService;
import com.github.people.utils.file_utils.FileUtils;
import com.mongodb.lang.NonNull;

import java.io.File;
import java.util.List;

public class FormatService implements IPeopleService {

    private final List<Person> people;

    private final IFormat format;

    private final String fileName;

    public FormatService(IFormat format, File file) {
        this.format = format;
        this.fileName = file.getName();
        this.people = format.fromFormat(FileUtils.readFile(fileName));
    }

    @Override
    public Person create(@NonNull Person createdPerson) {
        this.people.add(createdPerson);
        FileUtils.writeToFile(fileName, format.toFormat(this.people));
        return createdPerson;
    }

    @Override
    public List<Person> readAll() {
        return this.people;
    }

    @Override
    public void update(@NonNull Person updatedPerson) {
        for (int i = 0; i < this.people.size(); i++) {
            if (this.people.get(i).getId() == updatedPerson.getId()) {
                this.people.set(i, updatedPerson);
                FileUtils.writeToFile(fileName, format.toFormat(this.people));
                return;
            }
        }
    }

    @Override
    public void delete(long id) {
        this.people.removeIf(person -> person.getId() == id);
        FileUtils.writeToFile(fileName, format.toFormat(this.people));
    }

    public static IPeopleService getFormatService(File file) {
        String ext = file.getName().substring(file.getName().lastIndexOf('.'));

        switch (ext) {
            case ".json": {
                return new FormatService(new JsonFormat(), file);
            }
            case ".yml": {
                return new FormatService(new YmlFormat(), file);
            }
            case ".xml": {
                return new FormatService(new XmlFormat(), file);
            }
            case ".bin": {
                return new BinService(file);
            }
            case ".csv": {
                return new FormatService(new CsvFormat(), file);
            }
            default:
                throw new IllegalArgumentException("Extension of a file is not supported.");
        }
    }
}
