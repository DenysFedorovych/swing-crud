package com.github.people.services.impl.database;

import com.github.people.entity.Person;
import com.github.people.services.IPeopleService;

import java.util.ArrayList;
import java.util.List;

public class MockService implements IPeopleService {

    public List<Person> people = new ArrayList<>();

    @Override
    public Person create(Person createdPerson) {
        if(createdPerson == null){
            throw new IllegalArgumentException("Null argument exception.");
        }
        this.people.add(createdPerson);
        return createdPerson;
    }

    @Override
    public List<Person> readAll() {
        return this.people;
    }

    @Override
    public void update(Person updatedPerson) {
        if(updatedPerson == null) {
            throw new IllegalArgumentException("Null argument exception.");
        }
        for(int i = 0; i < this.people.size(); i++){
            if(this.people.get(i).getId() == updatedPerson.getId()){
                this.people.set(i, updatedPerson);
            }
        }
    }

    @Override
    public void delete(long id) {
        this.people.removeIf(person -> person.getId() == id);
    }
}
