package com.github.people.services.impl.files;

import com.github.people.entity.Person;
import com.github.people.formats.impl.BinFormat;
import com.github.people.services.IPeopleService;
import com.github.people.utils.file_utils.FileUtils;
import com.mongodb.lang.NonNull;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class BinService implements IPeopleService {

    private final List<Person> people;

    private final BinFormat format;

    private final String fileName;

    public BinService(File file) {
        this.fileName = file.getName();
        this.format = new BinFormat();
        this.people = format.fromFormat(FileUtils.readBinFile(this.fileName));
    }

    @Override
    public Person create(@NonNull Person createdPerson) {
        this.people.add(createdPerson);
        FileUtils.writeToBinFile(format.toFormat(this.people), this.fileName);
        return createdPerson;
    }

    @Override
    public List<Person> readAll() {
        return this.people;
    }

    @Override
    public void update(@NonNull Person updatedPerson) {
        for (int i = 0; i < this.people.size(); i++) {
            if (this.people.get(i).getId() == updatedPerson.getId()) {
                this.people.set(i, updatedPerson);
                FileUtils.writeToBinFile(format.toFormat(this.people), this.fileName);
            }
        }
    }

    @Override
    public void delete(long id) {
        for (Person person : this.people) {
            if (person.getId() == id) {
                this.people.remove(person);
                FileUtils.writeToBinFile(format.toFormat(this.people), this.fileName);
            }
        }
    }
}
