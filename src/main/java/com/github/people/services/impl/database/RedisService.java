package com.github.people.services.impl.database;

import com.github.people.entity.Person;
import com.github.people.repository.IPeopleRepo;
import com.github.people.services.IPeopleService;

import java.util.List;

public class RedisService implements IPeopleService {

    private final IPeopleRepo peopleRepo;

    private static RedisService classObject;

    private RedisService(IPeopleRepo peopleRepo) {
        this.peopleRepo = peopleRepo;
    }

    public static RedisService getRedisService(IPeopleRepo peopleRepo){
        if(classObject == null){classObject = new RedisService(peopleRepo);}
        return classObject;
    }

    @Override
    public Person create(Person person) {
        return peopleRepo.save(person);
    }

    @Override
    public List<Person> readAll() {
        return peopleRepo.findAll();
    }

    @Override
    public void update(Person person) {
        peopleRepo.update(person);
    }

    @Override
    public void delete(long id) {
        peopleRepo.remove(id);
    }
}
