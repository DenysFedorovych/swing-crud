package com.github.people.services.impl.database;

import com.github.people.entity.Person;
import com.github.people.repository.IPeopleRepo;
import com.github.people.services.IPeopleService;
import java.util.List;

public class MongoDBService implements IPeopleService {

    private final IPeopleRepo peopleRepo;

    private static MongoDBService classObject;

    private MongoDBService(IPeopleRepo peopleRepo) {
        this.peopleRepo = peopleRepo;
    }

    public static MongoDBService getMongoDBService(IPeopleRepo peopleRepo){
        if(classObject == null){classObject = new MongoDBService(peopleRepo);}
        return classObject;
    }

    @Override
    public Person create(Person person) {
        return peopleRepo.save(person);
    }

    @Override
    public List<Person> readAll() {
        return peopleRepo.findAll();
    }

    @Override
    public void update(Person person) {
        peopleRepo.update(person);
    }

    @Override
    public void delete(long id) {
        peopleRepo.remove(id);
    }
}
