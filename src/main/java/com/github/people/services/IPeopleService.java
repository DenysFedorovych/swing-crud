package com.github.people.services;

import com.github.people.entity.Person;

import java.util.List;

public interface IPeopleService {

    Person create(Person person);

    List<Person> readAll();

    void update(Person person);

    void delete(long id);

}
