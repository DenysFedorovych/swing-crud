package com.github.people.config;

import com.github.people.services.impl.files.FormatService;
import com.github.people.utils.file_utils.Paths;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class ServicesManagerTest {

    private String[] databasesArr = {"MySQL", "PostgreSQL", "H2", "MongoDB", "GraphDB", "Cassandra", "Redis"};

    private File json = new File(Paths.getJsonBasePath()+"file.json");

    private File csv = new File(Paths.getCsvBasePath()+"file.csv");

    private File xml = new File(Paths.getXmlBasePath()+"file.xml");

    private File bin = new File(Paths.getBinBasePath()+"file.bin");

    private File yml = new File(Paths.getYmlBasePath()+"file.yml");

    @Test
    public void getServiceJson() {
        Assert.assertEquals(ServicesManager.getService(json).readAll(), FormatService.getFormatService(json).readAll());
    }

    @Test
    public void getServiceXml() {
        Assert.assertEquals(ServicesManager.getService(xml).readAll(), FormatService.getFormatService(xml).readAll());
    }

    @Test
    public void getServiceYml() {
        Assert.assertEquals(ServicesManager.getService(yml).readAll(), FormatService.getFormatService(yml).readAll());
    }

    @Test
    public void getServiceCsv() {
        Assert.assertEquals(ServicesManager.getService(csv).readAll(), FormatService.getFormatService(csv).readAll());
    }

    @Test
    public void getServiceBin() {
        Assert.assertEquals(ServicesManager.getService(bin).readAll(), FormatService.getFormatService(bin).readAll());
    }

    @Test
    public void getServiceMySQL() {
        Assert.assertEquals(ServicesManager.getService(databasesArr[0]).toString(),ServicesConfig.mySQLPersonService().toString());
    }

    @Test
    public void getServicePostgresSQL() {
        Assert.assertEquals(ServicesManager.getService(databasesArr[1]).toString(),ServicesConfig.postgreSQLPersonService().toString());
    }

    @Test
    public void getServiceH2() {
        Assert.assertEquals(ServicesManager.getService(databasesArr[2]).toString(),ServicesConfig.h2PersonService().toString());
    }

    @Test
    public void getServiceMongoDB() {
        Assert.assertEquals(ServicesManager.getService(databasesArr[3]).toString(),ServicesConfig.mongoDBPersonService().toString());
    }

    @Test
    public void getServiceGraphDB() {
        Assert.assertEquals(ServicesManager.getService(databasesArr[4]).toString(),ServicesConfig.graphDBPersonService().toString());
    }

    @Test
    public void getServiceCassandra() {
        Assert.assertEquals(ServicesManager.getService(databasesArr[5]).toString(),ServicesConfig.cassandraPersonService().toString());
    }

    @Test
    public void getServiceRedis() {
        Assert.assertEquals(ServicesManager.getService(databasesArr[6]).toString(),ServicesConfig.redisPersonService().toString());
    }
}