package com.github.people.formats;

import com.github.people.entity.Person;
import com.github.people.formats.impl.JsonFormat;
import org.junit.Assert;
import org.junit.Test;


import java.util.ArrayList;
import java.util.List;


public class JsonFormatTest {
    private JsonFormat jf = new JsonFormat();
    private String onePerson = "{\"id\":2001203,\"firstName\":\"Denys\",\"lastName\":\"Fedorovych\",\"age\":18,\"city\":\"Kyiv\"}";
    private String twoPer = "[\n{\"id\":2001203,\"firstName\":\"Denys\",\"lastName\":\"Fedorovych\",\"age\":18,\"city\":\"Kyiv\"}, \"{\"id\":2001203,\"firstName\":\"Denys\",\"lastName\":\"Fedororvych\",\"age\":18,\"city\":\"Kyiv\"}\n]";
    private String personWithHeight = "{\"id\":2001203,\"firstName\":\"Denys\",\"lastName\":\"Fedorovych\",\"height\":18,\"city\":\"Kyiv\"}";
    private long id_expected = 2001203;

    @Test
    public void fromFormatTest() {

        List<Person> list = jf.fromFormat(onePerson);
        Assert.assertEquals(id_expected, list.get(0).getId());
        list = jf.fromFormat(twoPer);
        Assert.assertEquals(id_expected, list.get(0).getId());
        Assert.assertEquals(id_expected, list.get(1).getId());

    }

    @Test
    public void fromFormatToFormatTwoPeopleEquality() {

        List<Person> list = new ArrayList<>();
        list.add(new Person(10000, "Denys", "Fedorovych", 18, "Kyiv"));
        list.add(jf.fromFormat(onePerson).get(0));
        Assert.assertArrayEquals(list.toArray(), jf.fromFormat(jf.toFormat(list)).toArray());

    }

    @Test
    public void fromFormatToFormatOnePersonEquality() {

        List<Person> list = new ArrayList<>();
        list.add(new Person(10000, "Denys", "Fedorovych", 18, "Kyiv"));
        Assert.assertArrayEquals(list.toArray(), jf.fromFormat(jf.toFormat(list)).toArray());

    }

    @Test
    public void fromFormatBlankTest() {

        List<Person> list = new ArrayList<>();
        Assert.assertArrayEquals(list.toArray(), jf.fromFormat("{   }").toArray());
        Assert.assertArrayEquals(list.toArray(), jf.fromFormat("{\n}").toArray());
        Assert.assertArrayEquals(list.toArray(), jf.fromFormat("{   \n\t\n   }").toArray());

    }

    @Test
    public void toFormatEmptyListTest() {
        Assert.assertEquals("{}", jf.toFormat(new ArrayList<>()));
    }

}