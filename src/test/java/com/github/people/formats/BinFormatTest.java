package com.github.people.formats;

import com.github.people.entity.Person;
import com.github.people.formats.impl.BinFormat;
import org.junit.Assert;
import org.junit.Test;


import java.util.ArrayList;
import java.util.List;

public class BinFormatTest {

    private final BinFormat binFormat = new BinFormat();

    @Test
    public void binFromFormatToFormat(){
        Person firstPerson = new Person(1111111111111111111L, "Mark-2", "Eduardovich", 23, "Vladivostok");
        Person secondPerson = new Person(2222222222222222222L, "Willy", "Wonka", 47, "Beijing");
        ArrayList<Person> input = new ArrayList<>();
        input.add(firstPerson);
        input.add(secondPerson);

        List<Person> output = binFormat.fromFormat(binFormat.toFormat(input));

        Assert.assertEquals(input, output);
    }

    @Test
    public void fromFormatEmpty(){
        List<Person> exp = new ArrayList<>(0);
        List<Person> act = binFormat.fromFormat(new byte[0]);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void toFormatEmpty(){
        List<Person> people = new ArrayList<>(0);
        byte[] exp = new byte[0];
        byte[] act = binFormat.toFormat(people);
        Assert.assertArrayEquals(exp, act);
    }
}
