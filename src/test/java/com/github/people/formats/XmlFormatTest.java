package com.github.people.formats;

import com.github.people.entity.Person;
import com.github.people.formats.impl.XmlFormat;
import org.junit.Assert;
import org.junit.Test;


import java.util.ArrayList;
import java.util.List;

public class XmlFormatTest {

    private Person person = new Person(1231234213414244531L,"Denys","Fe",21,"Kyy");
    private XmlFormat xmlFormat = new XmlFormat();

    private String onePersonXml  = "<person>\n" +
            "\t\t<id>1231234213414244531</id>\n" +
            "\t\t<firstName>Denys</firstName>\n" +
            "\t\t<lastName>Fe</lastName>\n" +
            "\t\t<age>21</age>\n" +
            "\t\t<city>Kyy</city>\n" +
            "\t</person>";

    private String twoPersonXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<people>\n" +
            "\t<person>\n" +
            "\t\t<id>1231234213414244531</id>\n" +
            "\t\t<firstName>Denys</firstName>\n" +
            "\t\t<lastName>Fe</lastName>\n" +
            "\t\t<age>21</age>\n" +
            "\t\t<city>Kyy</city>\n" +
            "\t</person>\n" +
            "\t<person>\n" +
            "\t\t<id>1231234213414244531</id>\n" +
            "\t\t<firstName>Denys</firstName>\n" +
            "\t\t<lastName>Fe</lastName>\n" +
            "\t\t<age>21</age>\n" +
            "\t\t<city>Kyy</city>\n" +
            "\t</person>\n" +
            "</people>";

    @Test
    public void fromFormatOnePerson() {

        List<Person> personList = new ArrayList<>();
        personList.add(person);
        Assert.assertArrayEquals(personList.toArray(),xmlFormat.fromFormat(onePersonXml).toArray());

    }

    @Test
    public void fromFormatTwoPerson(){

        List<Person> personList = new ArrayList<>();
        personList.add(person);
        personList.add(person);
        Assert.assertArrayEquals(personList.toArray(),xmlFormat.fromFormat(twoPersonXml).toArray());

    }

    @Test
    public void toFormatOnePerson() {
        Assert.assertEquals(onePersonXml,xmlFormat.toFormat(person).trim());
    }

    @Test
    public void toFormatPeople() {

        List<Person> personList = new ArrayList<>();
        personList.add(person);
        personList.add(person);
        Assert.assertEquals(twoPersonXml,xmlFormat.toFormat(personList).trim());

    }

}